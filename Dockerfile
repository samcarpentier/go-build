FROM golang:alpine

RUN apk add --update \
  git \
  build-base

CMD [ "go", "version" ]
